<?php

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Logging\DebugStack;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use niKwitt\Utils\ConnectionManager;
use niKwitt\Utils\EntityManagerFactory;

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/config/defines.php';
require_once dirname(__DIR__) . '/config/env.defines.php';

try {
    ConnectionManager::dropAndCreateDatabase();
    $connection = ConnectionManager::createConnection();

    $stack = new DebugStack();
    $connection->getConfiguration()->setSQLLogger($stack);
    file_put_contents(ROOT_DIR . '/log/doctrine.sql.log', json_encode($stack), FILE_APPEND);

    return ConsoleRunner::createHelperSet(EntityManagerFactory::createEntityManager($connection));
} catch (DBALException $e) {
    echo $e->getMessage();
} catch (Exception $e) {
    echo $e->getMessage();
}
