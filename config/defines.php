<?php

define('ROOT_DIR', dirname(__DIR__));

define('SRC_DIR', ROOT_DIR . '/src');
define('APP_DIR', SRC_DIR . '/Application');
define('DOM_DIR', SRC_DIR . '/Domain');
define('STR_DIR', SRC_DIR . '/Infrastructure');

define('CONFIG_DIR', ROOT_DIR . '/config');
define('CERT_DIR', ROOT_DIR . '/certificate');
define('VENDOR_DIR', ROOT_DIR . '/vendor');
