<?php

require_once dirname(__DIR__) . '/config/defines.php';

$dotenv = Dotenv\Dotenv::create(ROOT_DIR);
$dotenv->load();

define('ENV', getenv('ENV'));

define('DB_DRIVER', getenv('DB_DRIVER'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASS', getenv('DB_PASS'));
define('DB_NAME', getenv('DB_NAME'));
define('DB_HOST', getenv('DB_HOST'));
