<?php

namespace niKwitt\Application;

use Slim\App as SlimApp;
use Slim\Factory\AppFactory;

/**
 * Class App.
 */
abstract class App
{
    private static $app;

    /**
     * Initialization of Slim Application in Singleton mode for all project.
     *
     * @return SlimApp
     */
    public static function getInstance(): SlimApp
    {
        if (self::$app === null) {
            self::$app = AppFactory::create();
        }

        return self::$app;
    }
}
