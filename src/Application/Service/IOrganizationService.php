<?php


namespace niKwitt\Application\Service;


use niKwitt\Domain\Entity\Organization;

interface IOrganizationService
{
    public static function add(Organization $organization): void;

    public static function update(Organization $organization): void;

    public static function remove(string $id): void;

    public static function findById(string $id): Organization;

}