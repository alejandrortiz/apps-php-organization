<?php

namespace niKwitt\Application\Service;

use Doctrine\ORM\ORMException;
use niKwitt\Domain\Entity\Organization;
use niKwitt\Infrastructure\Repository\DoctrineDepartmentRepository;

/**
 * Class OrganizationService.
 */
abstract class OrganizationService implements IOrganizationService
{
    /**
     * @var DoctrineDepartmentRepository
     */
    private static $repository;

    /**
     * @param Organization $organization
     *
     * @throws ORMException
     */
    public static function add(Organization $organization): void
    {
        self::createRepository();
        self::$repository->add($organization);
    }

    /**
     * @param Organization $organization
     *
     * @throws ORMException
     */
    public static function update(Organization $organization): void
    {
        self::createRepository();
        self::$repository->update($organization);
    }

    /**
     * @param string $id
     *
     * @return Organization
     */
    public static function findById(string $id): Organization
    {
        self::createRepository();

        return self::$repository->findById($id);
    }

    /**
     * @param string $id
     *
     * @throws ORMException
     */
    public static function remove(string $id): void
    {
        self::createRepository();
        self::$repository->remove($id);
    }

    /**
     * @return DoctrineDepartmentRepository
     */
    private static function createRepository(): DoctrineDepartmentRepository
    {
        if (is_null(self::$repository)) {
            self::$repository = new DoctrineDepartmentRepository();
        }

        return self::$repository;
    }
}
