<?php

namespace niKwitt\Domain\Entity\Auditor;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;

/**
 * @ORM\Embedded
 */
class Auditor
{
    /**
     * @var string
     *
     * @Column(name="created_at", type="datetime", nullable=false, options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $createdAt;

    /**
     * @var string
     *
     * @Column(name="created_by", type="string", nullable=false)
     */
    protected $createdBy;

    /**
     * @var string
     *
     * @Column(name="updated_at", type="datetime", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @Column(name="updated_by", type="string", nullable=true)
     */
    protected $updatedBy;

    /**
     * Auditor constructor.
     * @param string $createdAt
     * @param string $createdBy
     * @param string $updatedAt
     * @param string $updator
     */
    public function __construct(string $createdAt, string $createdBy, string $updatedAt, string $updator)
    {
        $this->createdAt = $createdAt;
        $this->createdBy = $createdBy;
        $this->updatedAt = $updatedAt;
        $this->updator = $updator;
    }

}
