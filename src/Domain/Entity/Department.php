<?php

namespace niKwitt\Domain\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\PersistentCollection;
use niKwitt\Domain\Entity\Auditor\Auditor;

/**
 * Class Department.
 *
 * @Entity(repositoryClass="niKwitt\Infrastructure\Repository\DoctrineDepartmentRepository")
 * @Table(name="departments")
 */
class Department extends Auditor
{
    /**
     * @var string
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false)
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $uuid;

    /**
     * @var string
     *
     * @Unique
     * @Column(type="string", length=250, unique=true, nullable=false)
     */
    private $name;

    /**
     * @var Department
     *
     * @Unique
     * @Column(type="string", length=250, unique=true, nullable=false)
     */
    private $parent;

    /**
     * @var Organization
     *
     * @Unique
     * @Column(type="string", length=250, unique=true, nullable=false)
     */
    private $organization;

    /**
     * @var PersistentCollection|Workplace[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Workplace",
     *     mappedBy="department",
     *     cascade={"remove"},
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true
     *     )
     */
    private $workplaces;

    /**
     * Department constructor.
     *
     * @param string          $uuid
     * @param string          $name
     * @param Organization    $organization
     * @param Department|null $parent
     */
    private function __construct(string $uuid, string $name, Organization $organization, Department $parent)
    {
        $this->uuid = $uuid;
        $this->name = $name;
        $this->organization = $organization;
        $this->parent = $parent;
    }

    public function addWorkplace(Workplace $workplace): bool
    {
        if (!empty($this->workplaces)) {
            foreach ($this->workplaces as $wPlace) {
                if ($workplace->getUuid() === $wPlace->getUuid() || $workplace->getName() === $wPlace->getName()) {
                    throw new \LogicException('This workplace already exists in this department');
                }
            }
        }

        $this->workplaces[] = $workplace;

        return true;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
