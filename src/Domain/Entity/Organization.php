<?php

namespace niKwitt\Domain\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\PersistentCollection;
use niKwitt\Domain\Entity\Auditor\Auditor;
use Ramsey\Uuid\Uuid;

/**
 * Class Organization.
 *
 * @Entity(repositoryClass="niKwitt\Infrastructure\Repository\DoctrineOrganizationRepository")
 * @Table(name="organizations")
 */
class Organization extends Auditor
{
    /**
     * @var Uuid
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false)
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $uuid;

    /**
     * @var string
     *
     * @Unique
     * @Column(type="string", length=250, unique=true, nullable=false)
     */
    private $name;

    /**
     * @var PersistentCollection|Department[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Deparment",
     *      mappedBy="organization",
     *      cascade={"remove"},
     *      fetch="EXTRA_LAZY",
     *      orphanRemoval=true
     *     )
     */
    private $deparments;

    /**
     * Organization constructor.
     *
     * @param string      $uuid
     * @param string|null $name
     */
    private function __construct(string $uuid, string $name)
    {
        $this->uuid = $uuid;
        $this->name = $name;
    }

    public function addDepartment(Department $department): bool
    {
        if (!empty($this->deparments)) {
            foreach ($this->deparments as $dep) {
                if ($dep->getUuid() === $department->getUuid() || $dep->getName() === $department->getName()) {
                    throw new \LogicException('This department already exists in this organization');
                }
            }
        }

        $this->deparments[] = $department;

        return true;
    }

    /**
     * @return Collection|Department[]
     */
    public function getDeparments(): Collection
    {
        return $this->deparments;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }
}
