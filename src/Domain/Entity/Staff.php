<?php

namespace niKwitt\Domain\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use niKwitt\Domain\Entity\Auditor\Auditor;

/**
 * Class Workplace.
 *
 * @Entity(repositoryClass="niKwitt\Infrastructure\Repository\DoctrineDepartmentRepository")
 * @Table(name="staffs")
 */
class Staff extends Auditor
{
    /**
     * @var string
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false)
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $uuid;

    /**
     * @var string
     *
     * @Unique
     * @Column(type="string", length=250, unique=true, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @Unique
     * @Column(name="first_name", type="string", length=250, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @Unique
     * @Column(name="middle_name", type="string", length=250)
     */
    private $middleName;

    /**
     * @var string
     *
     * @Unique
     * @Column(name="last_name", type="string", length=250, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @Unique
     * @Column(name="email", type="string", length=250, unique=true, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @Unique
     * @Column(type="string", length=250)
     */
    private $password;

    /**
     * Staff constructor.
     *
     * @param string $uuid
     * @param string $username
     * @param string $firstName
     * @param string $middleName
     * @param string $lastName
     * @param string $email
     * @param string $password
     */
    private function __construct(
        string $uuid,
        string $username,
        string $firstName,
        string $middleName,
        string $lastName,
        string $email,
        string $password
    ) {
        $this->uuid = $uuid;
        $this->username = $username;
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName(string $middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }
}
