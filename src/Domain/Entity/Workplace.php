<?php

namespace niKwitt\Domain\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\PersistentCollection;
use niKwitt\Domain\Entity\Auditor\Auditor;
use niKwitt\Domain\ValueObject\PrimaryKey;

/**
 * Class Workplace.
 *
 * @Entity(repositoryClass="niKwitt\Infrastructure\Repository\DoctrineDepartmentRepository")
 * @Table(name="workplaces")
 */
class Workplace extends Auditor
{
    /**
     * @var string
     *
     * @Id
     * @Column(type="uuid", unique=true, nullable=false)
     * @CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $uuid;

    /**
     * @var string
     *
     * @Unique
     * @Column(type="string", length=250, unique=true, nullable=false)
     */
    private $name;

    /**
     * @var Workplace|null
     *
     * @Unique
     * @Column(type="string", length=250, unique=true, nullable=true)
     */
    private $parent;

    /**
     * @var Department
     *
     * @Unique
     * @Column(type="string", length=250, unique=true, nullable=false)
     */
    private $department;

    /**
     * @var PersistentCollection|Staff[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Staff",
     *     mappedBy="workplace",
     *     cascade={"remove"},
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true
     *     )
     */
    private $staffs;

    /**
     * Workplace constructor.
     *
     * @param PrimaryKey     $uuid
     * @param string         $name
     * @param Department     $department
     * @param Workplace|null $parent
     */
    public function __construct(PrimaryKey $uuid, string $name, ?Department $department, Workplace $parent)
    {
        $this->uuid = $uuid;
        $this->name = $name;
        $this->department = $department;
        $this->parent = $parent;
    }

    public function addStaff(Staff $staff): bool
    {
        if (!empty($this->staffs)) {
            foreach ($this->staffs as $obj) {
                if ($obj->getUuid() === $staff->getUuid() || $obj->getUsername() === $staff->getUsername()) {
                    throw new \LogicException('This staff already exists in this workplace');
                }
            }
        }

        $this->staffs[] = $staff;

        return true;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
