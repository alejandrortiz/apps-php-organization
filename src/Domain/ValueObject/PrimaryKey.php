<?php

namespace niKwitt\Domain\ValueObject;

use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @property UuidInterface pk
 */
final class PrimaryKey
{
    /**
     * @var UuidInterface
     */
    private $primaryKey;

    public function __construct()
    {
        try {
            $this->pk = Uuid::uuid4();
        } catch (Exception $e) {
            /*
             * @PROJECT: LOGGER
             */
        }
    }

    /**
     * @return UuidInterface
     */
    public function get(): UuidInterface
    {
        return $this->pk;
    }
}
