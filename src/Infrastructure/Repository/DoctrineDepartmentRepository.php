<?php

namespace niKwitt\Infrastructure\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;
use niKwitt\Domain\Entity\Organization;
use niKwitt\Domain\Repository\IDepartmentRepository;

/**
 * Class DoctrineOrganizationRepository.
 */
class DoctrineDepartmentRepository implements IDepartmentRepository
{
    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $uuid
     *
     * @return Organization
     *
     * @throws Exception
     */
    public function findById(string $uuid)
    {
        return $this->getThrowingException($uuid);
    }

    /**
     * @param Organization $organization
     *
     * @throws ORMException
     */
    public function add(Organization $organization): void
    {
        $this->entityManager->persist($organization);
    }

    /**
     * @param Organization $organization
     *
     * @throws ORMException
     */
    public function update(Organization $organization): void
    {
        $this->entityManager->refresh($organization);
    }

    /**
     * @param string $uuid
     *
     * @throws ORMException
     * @throws Exception
     */
    public function remove(string $uuid)
    {
        $organization = $this->getThrowingException($uuid);
        $this->entityManager->remove($organization);
    }

    /**
     * @param string $uuid
     *
     * @return Organization
     *
     * @throws Exception
     */
    private function getThrowingException(string $uuid): Organization
    {
        $organization = $this->findById($uuid);

        if ($organization instanceof Organization) {
            return $organization;
        }

        throw new Exception('Organization not found');
    }
}
