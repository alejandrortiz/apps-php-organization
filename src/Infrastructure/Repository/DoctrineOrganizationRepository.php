<?php

namespace niKwitt\Infrastructure\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use niKwitt\Domain\Entity\Organization;
use niKwitt\Domain\Repository\IOrganizationRepository;

/**
 * Class DoctrineOrganizationRepository.
 */
class DoctrineOrganizationRepository implements IOrganizationRepository
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $uuid
     *
     * @return Organization
     */
    public function findById(string $uuid)
    {
        return $this->getThrowingException($uuid);
    }

    /**
     * @param Organization $organization
     *
     * @throws ORMException
     */
    public function add(Organization $organization): void
    {
        $this->entityManager->persist($organization);
    }

    /**
     * @param Organization $organization
     *
     * @throws ORMException
     */
    public function update(Organization $organization): void
    {
        $this->entityManager->refresh($organization);
    }

    /**
     * @param string $uuid
     *
     * @throws ORMException
     */
    public function remove(string $uuid)
    {
        $organization = $this->getThrowingException($uuid);
        $this->entityManager->remove($organization);
    }

    /**
     * @param string $uuid
     *
     * @return Organization
     */
    private function getThrowingException(string $uuid): Organization
    {
        $organization = $this->find($uuid);

        if ($organization instanceof Organization) {
            return $organization;
        }

        throw new Exception('Organization not found');
    }
}
