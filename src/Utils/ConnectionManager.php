<?php

namespace niKwitt\Utils;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver;
use Doctrine\DBAL\Driver\PDOMySql\Driver as MySqlDriver;
use Doctrine\DBAL\Driver\PDOSqlite\Driver as SqliteDriver;
use Exception;

/**
 * Class ConnectionManager.
 */
class ConnectionManager
{
    /**
     * @var Connection
     */
    private static $connectionToCreateDatabase;

    /**
     * @throws DBALException
     * @throws Exception
     */
    public static function dropAndCreateDatabase(): void
    {
        if (empty(self::$connectionToCreateDatabase)) {
            self::$connectionToCreateDatabase = new Connection([
                'user' => self::getUser(),
                'password' => self::getPassword(),
                'host' => self::getHost(),
            ], self::getDriver());
        }

        self::$connectionToCreateDatabase->exec(sprintf('DROP DATABASE IF EXISTS %s', self::getDbName()));
        self::$connectionToCreateDatabase->exec(sprintf('CREATE DATABASE %s', self::getDbName()));
    }

    /**
     * @return string|null
     */
    private static function getUser(): ?string
    {
        return DB_USER ?? null;
    }

    /**
     * @return string|null
     */
    private static function getPassword(): ?string
    {
        return DB_PASS ?? null;
    }

    /**
     * @return string|null
     */
    private static function getDbName(): ?string
    {
        return DB_NAME ?? null;
    }

    /**
     * @return string|null
     */
    private static function getHost(): ?string
    {
        return DB_HOST ?? null;
    }

    /**
     * @return Driver
     *
     * @throws Exception
     */
    private static function getDriver(): Driver
    {
        $dbDriver = DB_DRIVER;

        if (!isset($dbDriver)) {
            throw new Exception('Please set DB_DRIVER in global config');
        }
        if ('pdo_mysql' === $dbDriver) {
            return new MySqlDriver();
        }
        throw new Exception(sprintf('DB_DRIVER "%s" not supported', $dbDriver));
    }

    /**
     * @return Connection
     *
     * @throws DBALException
     * @throws Exception
     */
    public static function createConnection(): Connection
    {
        return new Connection([
            'dbname' => self::getDbName(),
            'user' => self::getUser(),
            'password' => self::getPassword(),
            'host' => self::getHost(),
        ], self::getDriver());
    }

    /**
     * @return Connection
     *
     * @throws DBALException
     */
    public static function createSqliteMemoryConnection()
    {
        return new Connection(['memory' => true], new SqliteDriver());
    }
}
