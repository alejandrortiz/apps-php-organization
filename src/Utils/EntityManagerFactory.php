<?php

namespace niKwitt\Utils;

use Doctrine\Common\Cache\ApcuCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

class EntityManagerFactory
{
    /**
     * @param Connection $connection
     *
     * @return EntityManager
     *
     * @throws ORMException
     * @throws DBALException
     */
    public static function createEntityManager(Connection $connection): EntityManager
    {
        $isDevMode = 'dev' === ENV;

        $cache = $isDevMode ? new ArrayCache() : new ApcuCache();

        $config = new Configuration();
        $config->setMetadataCacheImpl($cache);
        $driverImpl = $config->newDefaultAnnotationDriver(dirname(__DIR__) . '/Domain');
        $config->setMetadataDriverImpl($driverImpl);
        $config->setQueryCacheImpl($cache);

        $config->setProxyDir(dirname(__DIR__) . '/../tmp/Cache');
        $config->setProxyNamespace('Doctrine\Tests\Proxies');
        $config->setAutoGenerateProxyClasses($isDevMode);

        if (!Type::hasType('uuid')) {
            Type::addType('uuid', 'Ramsey\Uuid\Doctrine\UuidType');
        }

        return EntityManager::create($connection, $config);
    }
}
