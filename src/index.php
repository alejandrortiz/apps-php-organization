<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config/defines.php';

require_once ROOT_DIR . '/config/env.defines.php';

use niKwitt\Application\App;
use niKwitt\Application\Controller\Organization\OrganizationRoute;

$app = App::getInstance();

$app->addRoutingMiddleware();

/*
 * Add Error Handling Middleware
 *
 * @param bool $displayErrorDetails -> Should be set to false in production
 * @param bool $logErrors -> Parameter is passed to the default ErrorHandler
 * @param bool $logErrorDetails -> Display error details in error log
 * which can be replaced by a callable of your choice.

 * Note: This middleware should be added last. It will not handle any exceptions/errors
 * for middleware added after it.
 */
$app->addErrorMiddleware(true, true, true);

$app->get('/{name}', OrganizationRoute::class . ':getAll');

$app->run();
